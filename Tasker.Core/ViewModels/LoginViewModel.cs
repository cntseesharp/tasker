﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Tasker.Core.Interfaces;
using Tasker.Domain;

namespace Tasker.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private ILoginService _service;

        public delegate void LoginSuccededEvent(object sender, EventArgs args);
        public event LoginSuccededEvent LoggedIn;

        protected virtual void OnSuccessLogin()
        {
            LoggedIn?.Invoke(this, EventArgs.Empty);
        }

        public LoginViewModel(ILoginService taskService)
        {
            _service = taskService;
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public IMvxCommand LoginCommand
        {
            get
            {
                return new MvxCommand<TaskEntity>(async task =>
                {
                    //await System.Threading.Tasks.Task.Delay(2000); //Like we're doing something important :D
                    if (_service.LoginAttempt(Username, Password))
                    {
                        OnSuccessLogin();
                        ShowViewModel<TaskListViewModel>();
                    }
                });
            }
        }
    }
}
