﻿using MvvmCross.Core.ViewModels;
using PCLStorage;
using System.IO;
using System.Reflection;
using Tasker.Core.Interfaces;
using Tasker.Core.Services;
using Tasker.Domain;

namespace Tasker.Core.ViewModels
{
    public class TaskViewModel : MvxViewModel
    {
        private TaskEntity _task;
        private ITaskService _service;
        private byte[] _image;

        public TaskViewModel()
        {
            _service = new TaskService();
        }

        public async void Init(Parameter param)
        {
            _task = _service.GetTask(param.ID) ?? new TaskEntity() { ID = 0, Content = "", Title = "", IsDone = false };
            IFile file = await FileSystem.Current.GetFileFromPathAsync(string.Format("{0}/image{1}.dat", App.DataFolder, _task.ID));
            if (file != null)
            {
                using (Stream sw = await file.OpenAsync(FileAccess.Read))
                {
                    _image = new byte[sw.Length];
                    sw.Read(_image, 0, (int)sw.Length);
                    RaisePropertyChanged(() => TaskImage);
                }
            }
        }

        public void SaveImage(byte[] image)
        {
            IFile file = FileSystem.Current.GetFolderFromPathAsync(App.DataFolder).Result.CreateFileAsync(string.Format("image{0}.dat", _task.ID), CreationCollisionOption.ReplaceExisting).Result;
            using (Stream sw = file.OpenAsync(FileAccess.ReadAndWrite).Result)
            {
                sw.Write(image, 0, image.Length);
            }
            
            _image = image;
            _task.Image = string.Format("{0}/image{1}.dat", App.DataFolder, _task.ID);
            RaisePropertyChanged(() => TaskImage);
        }

        public bool ShowAddButton { get; set; } = false;

        public int Id { get { return _task.ID; } set { } }

        public string Title
        {
            get
            {
                return _task.Title;
            }
            set
            {
                _task.Title = value;
            }
        }

        public string Content
        {
            get
            {
                return _task.Content;
            }
            set
            {
                _task.Content = value;
            }
        }

        public bool IsDone
        {
            get
            {
                return _task.IsDone;
            }
            set
            {
                _task.IsDone = value;
            }
        }

        public byte[] TaskImage
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value; RaisePropertyChanged(() => TaskImage);
            }
        }

        public IMvxCommand SaveCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    if (_task.ID == 0)
                        _service.AddTask(_task);
                    _service.UpdateTask(_task);
                    Close(this);
                });
            }
        }

        public IMvxCommand DeleteCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    _service.DeleteTask(_task);
                    Close(this);
                });
            }
        }

        public class Parameter
        {
            public int ID { get; set; }
        }

    }
}
