﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Tasker.Core.Interfaces;
using Tasker.Domain;

namespace Tasker.Core.ViewModels
{
    public class TaskListViewModel : MvxViewModel
    {
        private ITaskService _service;
        public delegate void RefreshRequestedEvent(object sender, EventArgs args);
        public event RefreshRequestedEvent Refresh;

        protected virtual void OnRefresh()
        {
            Refresh?.Invoke(this, EventArgs.Empty);
        }

        public TaskListViewModel(ITaskService taskService)
        {
            _service = taskService;
        }

        public override void Start()
        {
            base.Start();
        }

        public bool ShowAddButton { get; set; } = true;

        public IEnumerable<TaskEntity> Tasks
        {
            get
            {
                return _service.GetTasks();
            }
        }

        public IMvxCommand ItemSelectedCommand
        {
            get
            {
                return new MvxCommand<TaskEntity>(task =>
                {
                    ShowViewModel<TaskViewModel>(new TaskViewModel.Parameter() { ID = task.ID });
                });
            }
        }

        public IMvxCommand AddNewTask
        {
            get
            {
                return new MvxCommand<TaskEntity>(task =>
                {
                    ShowViewModel<TaskViewModel>(new TaskViewModel.Parameter() { ID = 0 });
                });
            }
        }

        public IMvxCommand RefreshListCommand
        {
            get
            {
                return new MvxCommand(OnRefresh);
            }
        }
    }
}
