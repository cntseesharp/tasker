﻿using SQLite;
using Tasker.Domain;

namespace Tasker.Core.Services
{
    public class DatabaseService
    {
        static SQLiteConnection connection = null;

        public DatabaseService()
        {
            connection = new SQLiteConnection(App.DataFolder + "/data.sqlite3");
            connection.CreateTable<TaskEntity>();
            connection.CreateTable<User>();

            if (connection.Table<User>().Count() < 1)
                connection.Insert(new User() { Username = "test", PasswordHash = "dd0946cb7f04ed2e84395a9aac23b93b4767ef0d", Salt = "581" });

            if (connection.Table<TaskEntity>().Count() < 1)
                connection.Insert(new TaskEntity() { Content = "Test record content", Title = "First ever task", IsDone = true });
        }

        public SQLiteConnection Connection
        {
            get
            {
                return connection;
            }
            private set { }
        }

        public User SelectUser(string username)
        {
            return Connection.Table<User>().FirstOrDefault(x => x.Username.ToLower() == username);
        }
    }
}
