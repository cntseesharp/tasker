﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasker.Core.Interfaces;
using Tasker.Domain;

namespace Tasker.Core.Services
{
    public class TaskService : ITaskService
    {
        DatabaseService _db;

        public TaskService()
        {
            _db = new DatabaseService();
        }

        public void AddTask(TaskEntity task)
        {
            _db.Connection.Insert(task);
        }

        public void UpdateTask(TaskEntity task)
        {
            _db.Connection.Update(task);
        }

        public void DeleteTask(TaskEntity task)
        {
            _db.Connection.Delete(task);
        }

        public  IEnumerable<TaskEntity> GetTasks()
        {
            return _db.Connection.Table<TaskEntity>().ToList();
        }

        public TaskEntity GetTask(int id)
        {
            return _db.Connection.Table<TaskEntity>().FirstOrDefault(x => x.ID == id);
        }
    }
}
