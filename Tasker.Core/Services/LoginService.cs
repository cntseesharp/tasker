﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasker.Core.Interfaces;
using Tasker.Domain;
using PCLCrypto;

namespace Tasker.Core.Services
{
    public class LoginService : ILoginService
    {
        DatabaseService _db;

        public LoginService()
        {
            _db = new DatabaseService();
        }

        public bool CreateUser(string username, string password)
        {
            username = username.ToLower();

            if (_db.SelectUser(username) != null)
                return false;

            string salt = Convert.ToString(new Random().Next());
            string passwordHash = GenerateHash(username, password, salt);
            try
            {
                _db.Connection.Insert(new User() { Username = username, PasswordHash = passwordHash, Salt = salt });
            }
            catch
            {
                return false;
            }

            return true;
        }

        //Вообще я бы передавал кортеж из bool и User, но в 6 еще нет кортежей :с
        public bool LoginAttempt(string username, string password)
        {
            User user = _db.SelectUser(username);
            if (user == null)
                return false;

            string hash = GenerateHash(username, password, user.Salt);
            if (user.PasswordHash == hash)
                return true;

            return false;
        }

        string GenerateHash(string username, string password, string salt)
        {
            byte[] data = Encoding.UTF8.GetBytes(salt + username + password);
            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha1);
            byte[] hash = hasher.HashData(data);

            return BitConverter.ToString(hash).Replace("-", "").ToLower();
        }
    }
}
