﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasker.Core.Interfaces;
using Tasker.Core.Services;
using Tasker.Core.ViewModels;

namespace Tasker.Core
{
    public class App : MvxApplication
    {
        static public string DataFolder { get; set; }

        public App(string dataFolder)
        {
            DataFolder = dataFolder;
            Mvx.RegisterType<ITaskService, TaskService>();
            Mvx.RegisterType<ILoginService, LoginService>();
            Mvx.RegisterSingleton<IMvxAppStart>(new MvxAppStart<LoginViewModel>());
        }
    }
}