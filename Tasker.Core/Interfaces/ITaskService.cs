﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasker.Domain;

namespace Tasker.Core.Interfaces
{
    public interface ITaskService
    {
        void AddTask(TaskEntity task);
        void UpdateTask(TaskEntity task);
        void DeleteTask(TaskEntity task);
        IEnumerable<TaskEntity> GetTasks();
        TaskEntity GetTask(int id);
    }
}
