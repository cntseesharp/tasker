﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasker.Core.Interfaces
{
    public interface ILoginService
    {
        bool CreateUser(string username, string password);
        bool LoginAttempt(string username, string password);
    }
}
