﻿using SQLite;
using System.Diagnostics;

namespace Tasker.Domain
{
    [DebuggerDisplay("[Person: ID={ID}, Title={Title}, IsDone={IsDone}]")]
    public class TaskEntity
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsDone { get; set; }
        public string Image { get; set; }
    }
}
