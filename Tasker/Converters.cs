﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platform.Converters;
using Android.Graphics;
using System.IO;
using System.Globalization;

namespace Tasker
{
    public class ByteToBitmapConverter : MvxValueConverter<byte[], Bitmap>
    {
        protected override Bitmap Convert(byte[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.Length == 0) { return null; }
            return BitmapFactory.DecodeByteArray(value, 0, value.Length);
        }

        protected override byte[] ConvertBack(Bitmap value, Type targetType, object parameter, CultureInfo culture)
        {
            var stream = new MemoryStream();
            value.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
            return stream.ToArray();
        }
    }

    public class PathToBitmapConverter : MvxValueConverter<string, Bitmap>
    {
        protected override Bitmap Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
                return BitmapFactory.DecodeFile(value);
        }

        protected override string ConvertBack(Bitmap value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }
}