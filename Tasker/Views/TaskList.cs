using Android.App;
using Android.Support.V7.Widget;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views;
using System;
using System.Collections.Generic;
using Tasker.Core.ViewModels;
using Tasker.Domain;

namespace Tasker.Views
{
    [Activity(Label = "@string/ActivityNameTaskList")]
    public class TaskList : MvxActivity
    {
        public TaskList()
        {

        }

        private void RefreshRequested(object source, EventArgs args)
        {
            FindViewById<MvxListView>(Resource.Id.uiListView).ItemsSource = (ViewModel as TaskListViewModel).Tasks;
            FindViewById<MvxSwipeRefreshLayout>(Resource.Id.uiSwipeRefreshLayout).Refreshing = false;
        }

        protected override void OnViewModelSet()
        {
            SetContentView(Resource.Layout.TaskList);
            (ViewModel as TaskListViewModel).Refresh += RefreshRequested;
            var toolbarBottom = FindViewById<Toolbar>(Resource.Id.UIToolbar);
            toolbarBottom.Title = "Tasks list";
        }
        
        public override void OnBackPressed()
        {
            this.FinishAffinity();
        }
    }
}