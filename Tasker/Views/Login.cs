using Android.App;
using Android.Content;
using Android.Preferences;
using MvvmCross.Droid.Views;
using Tasker.Core.ViewModels;
using System;
using Android.Widget;

namespace Tasker.Views
{
    [Activity(Label = "@string/ActivityNameTaskList", MainLauncher = true)]
    public class Login : MvxActivity
    {
        public Login()
        {

        }

        protected override void OnViewModelSet()
        {
            SetContentView(Resource.Layout.Login);
            (ViewModel as LoginViewModel).LoggedIn += LoginSucceeded;

            var preferences = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
            FindViewById<EditText>(Resource.Id.uiUsername).Text = preferences.GetString("username", "");
            FindViewById<EditText>(Resource.Id.uiPassword).Text = preferences.GetString("password", "");
            FindViewById<CheckBox>(Resource.Id.uiRememberMe).Checked = preferences.GetBoolean("rememberMe", false);

            if (FindViewById<CheckBox>(Resource.Id.uiRememberMe).Checked)
                FindViewById<Button>(Resource.Id.uiLoginButton).PerformClick();
        }

        private void LoginSucceeded(object sender, EventArgs args)
        {
            var preferenceEdit = PreferenceManager.GetDefaultSharedPreferences(Application.Context).Edit();
            preferenceEdit.PutString("username", "").Commit();
            preferenceEdit.PutString("password", "").Commit();
            preferenceEdit.PutBoolean("rememberMe", FindViewById<CheckBox>(Resource.Id.uiRememberMe).Checked).Commit();
            if (FindViewById<CheckBox>(Resource.Id.uiRememberMe).Checked)
            {
                preferenceEdit.PutString("username", FindViewById<EditText>(Resource.Id.uiUsername).Text).Commit();
                preferenceEdit.PutString("password", FindViewById<EditText>(Resource.Id.uiPassword).Text).Commit();
            }
        }
    }
}