using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Support.V4.App;
using Android.Widget;
using Java.IO;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Views;
using System;
using System.IO;
using Tasker.Core.ViewModels;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Tasker.Views
{
    [Activity]
    public class Task : MvxActivity
    {

        private const int CameraIntent = 0;
        private const int GalleryIntent = 1;
        public Task()
        {

        }

        protected override void OnViewModelSet()
        {
            SetContentView(Resource.Layout.Task);
            var toolbarBottom = FindViewById<Toolbar>(Resource.Id.UIToolbar);
            toolbarBottom.Title = "Task";
            FindViewById<MvxImageView>(Resource.Id.uiTaskImageView).Click += Task_Click;
            if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new String[] { Android.Manifest.Permission.ReadExternalStorage }, 0);
            }
            if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new String[] { Android.Manifest.Permission.WriteExternalStorage }, 0);
            }
            Java.IO.File appDirectory = Android.OS.Environment.GetExternalStoragePublicDirectory("Tasker");
            if (!appDirectory.Exists())
                appDirectory.Mkdir();
        }

        private void Task_Click(object sender, EventArgs e)
        {
            string[] items = { "Take a photo", "Select from gallery" };
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle("Select image");
            alert.SetItems(items, (source, args) =>
            {
                if (args.Which == 0)
                {
                    if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.Camera) != (int)Permission.Granted)
                        ActivityCompat.RequestPermissions(this, new String[] { Android.Manifest.Permission.Camera }, 0);

                    Intent intent = new Intent(MediaStore.ActionImageCapture);
                    StartActivityForResult(intent, CameraIntent);
                }
                if (args.Which == 1)
                {
                    Intent intent = new Intent(MediaStore.IntentActionMediaSearch);
                    intent.SetType("image/*");
                    intent.SetAction(Intent.ActionGetContent);
                    StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), GalleryIntent);
                }
            });
            alert.Show();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode != Result.Ok)
                return;

            string environmentPath = Android.OS.Environment.GetExternalStoragePublicDirectory("Tasker").Path;
            if (requestCode == CameraIntent)
            {
                using (MemoryStream stream = new MemoryStream())
                    {
                    (data.Extras.Get("data") as Bitmap).Compress(Bitmap.CompressFormat.Png, 100, stream);
                    (this.ViewModel as TaskViewModel).SaveImage(environmentPath, stream.ToArray());
                }
            }

            if (requestCode == GalleryIntent)
            {
                using (var stream = ContentResolver.OpenInputStream(data.Data))
                {
                    MemoryStream mStream = new MemoryStream();
                    BitmapFactory.DecodeStream(stream).Compress(Bitmap.CompressFormat.Png, 100, mStream);
                    (this.ViewModel as TaskViewModel).SaveImage(environmentPath, mStream.ToArray());
                    mStream.Close();
                    mStream.Dispose();
                }

            }
        }

    }
}