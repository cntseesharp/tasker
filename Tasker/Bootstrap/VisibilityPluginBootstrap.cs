using MvvmCross.Platform.Plugins;

namespace Tasker.Bootstrap
{
    public class VisibilityPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Visibility.PluginLoader>
    {
    }
}