using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Java.IO;
using Java.Security;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using System;
using Tasker.Core;

namespace Tasker
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App(System.Environment.CurrentDirectory);
        }
    }
}