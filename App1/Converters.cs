﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmCross.Platform.Converters;
using System.IO;
using System.Globalization;
using UIKit;
using Foundation;

namespace Tasker
{
    public class ByteToImageConverter : MvxValueConverter<byte[], UIImage>
    {
        protected override UIImage Convert(byte[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.Length == 0) { return null; }
            return UIImage.LoadFromData(NSData.FromArray(value));
        }

        protected override byte[] ConvertBack(UIImage value, Type targetType, object parameter, CultureInfo culture)
        {
            using (NSData imageData = value.AsPNG())
            {
                Byte[] output = new Byte[imageData.Length];
                System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, output, 0, (int)imageData.Length);
                return output.ToArray();
            }
        }
    }

    public class PathToImageConverter : MvxValueConverter<string, UIImage>
    {
        protected override UIImage Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
                return UIImage.FromFile(value);
        }

        protected override string ConvertBack(UIImage value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }
}