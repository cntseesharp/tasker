﻿using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using System;
using Tasker.Core.ViewModels;
using UIKit;

namespace App1.Views
{
    public partial class LoginView : MvxViewController<LoginViewModel>
    {
        private NSUserDefaults _preferences = NSUserDefaults.StandardUserDefaults;
        public LoginView() : base("LoginView", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.ViewModel.LoggedIn += ViewModel_LoggedIn;

            ViewModel.Username = _preferences.StringForKey("username");
            ViewModel.Password = _preferences.StringForKey("password");
            uiRememberMe.On = _preferences.BoolForKey("rememberMe");

            this.CreateBinding(uiLoginInput).To((LoginViewModel vm) => vm.Username).Apply();
            this.CreateBinding(uiPasswordInput).To((LoginViewModel vm) => vm.Password).Apply();
            this.CreateBinding(uiLoginButton).To((LoginViewModel vm) => vm.LoginCommand).Apply();

            // Perform any additional setup after loading the view, typically from a nib.
        }

        private void ViewModel_LoggedIn(object sender, EventArgs args)
        {
            _preferences.SetString("", "username");
            _preferences.SetString("", "password");
            _preferences.SetBool(uiRememberMe.On, "rememberMe");
            if (uiRememberMe.On)
            {
                _preferences.SetString(uiLoginInput.Text, "username");
                _preferences.SetString(uiPasswordInput.Text, "password");
            }
        }
    }
}