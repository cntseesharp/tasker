﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace App1.Views
{
    [Register ("TaskListView")]
    partial class TaskListView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView uiTaskList { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (uiTaskList != null) {
                uiTaskList.Dispose ();
                uiTaskList = null;
            }
        }
    }
}