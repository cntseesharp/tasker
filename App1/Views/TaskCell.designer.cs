﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace App1.Views
{
    [Register ("TaskCell")]
    partial class TaskCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MvvmCross.Binding.iOS.Views.MvxImageView uiTableCellImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch uiTableCellSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel uiTableCellText { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (uiTableCellImage != null) {
                uiTableCellImage.Dispose ();
                uiTableCellImage = null;
            }

            if (uiTableCellSwitch != null) {
                uiTableCellSwitch.Dispose ();
                uiTableCellSwitch = null;
            }

            if (uiTableCellText != null) {
                uiTableCellText.Dispose ();
                uiTableCellText = null;
            }
        }
    }
}