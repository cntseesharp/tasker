﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace App1.Views
{
    [Register ("LoginView")]
    partial class LoginView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton uiLoginButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField uiLoginInput { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField uiPasswordInput { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch uiRememberMe { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (uiLoginButton != null) {
                uiLoginButton.Dispose ();
                uiLoginButton = null;
            }

            if (uiLoginInput != null) {
                uiLoginInput.Dispose ();
                uiLoginInput = null;
            }

            if (uiPasswordInput != null) {
                uiPasswordInput.Dispose ();
                uiPasswordInput = null;
            }

            if (uiRememberMe != null) {
                uiRememberMe.Dispose ();
                uiRememberMe = null;
            }
        }
    }
}