﻿using System;
using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using Tasker.Domain;

namespace App1.Views
{
    public partial class TaskCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("TaskCell");
        public static readonly UINib Nib;

        static TaskCell()
        {
            Nib = UINib.FromName("TaskCell", NSBundle.MainBundle);
        }

        protected TaskCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<TaskCell, TaskEntity>();
                set.Bind(uiTableCellText).To(m => m.Title);
                set.Bind(uiTableCellImage).To(m => m.Image).WithConversion("PathToImage");
                set.Bind(uiTableCellSwitch).To(m => m.IsDone);
                set.Apply();
            });
        }
    }
}
