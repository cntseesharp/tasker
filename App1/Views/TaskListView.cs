﻿using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using System;
using System.Collections.Generic;
using Tasker.Core.ViewModels;
using UIKit;

namespace App1.Views
{
    public partial class TaskListView : MvxViewController<TaskListViewModel>
    {
        public TaskListView() : base("TaskListView", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationItem.Title = "Task list";

            var source = new MvxSimpleTableViewSource(uiTaskList, "TaskCell", TaskCell.Key); //Bitmap Image,Converter=PathToBitmap;

            var set = this.CreateBindingSet<TaskListView, TaskListViewModel>();
            set.Bind(source).To(vm => vm.Tasks);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.ItemSelectedCommand);
            set.Apply();

            uiTaskList.Source = source;
            uiTaskList.ReloadData();
        }
    }
}