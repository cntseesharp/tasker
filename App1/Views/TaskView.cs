﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using System;
using Tasker.Core.ViewModels;
using UIKit;

namespace App1.Views
{
    public partial class TaskView : MvxViewController<TaskViewModel>
    {
        public TaskView() : base("TaskView", null)
        {

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.CreateBinding(uiTitleInput).To((TaskViewModel vm) => vm.Title).Apply();
            this.CreateBinding(uiContentInput).To((TaskViewModel vm) => vm.Content).Apply();
            this.CreateBinding(uiIsDoneSwitch).To((TaskViewModel vm) => vm.IsDone).Apply();
            this.CreateBinding(uiTaskImage).To((TaskViewModel vm) => vm.TaskImage).WithConversion("ByteToImage").Apply();
            this.CreateBinding(uiSaveButton).To((TaskViewModel vm) => vm.SaveCommand).Apply();
            this.CreateBinding(uiDeleteButton).To((TaskViewModel vm) => vm.DeleteCommand).Apply();

            uiSelectImage.TouchUpInside += OnTap;
        }

        private void OnTap(object sender, EventArgs e)
        {
            UIAlertView alert = new UIAlertView()
            {
                Title = "Loading picture",
                Message = "Select image source"
            };
            alert.AddButton("Camera");
            alert.AddButton("Gallery");
            alert.AddButton("Cancel");
            alert.Clicked += Alert_Clicked;
            alert.Show();
        }

        private void Alert_Clicked(object sender, UIButtonEventArgs e)
        {
            if(e.ButtonIndex == 0)
            {
                //Camera
            }
            if(e.ButtonIndex == 1)
            {

            }
        }
    }
}