﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace App1.Views
{
    [Register ("TaskView")]
    partial class TaskView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField uiContentInput { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton uiDeleteButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch uiIsDoneSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton uiSaveButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton uiSelectImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MvvmCross.Binding.iOS.Views.MvxImageView uiTaskImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField uiTitleInput { get; set; }

        [Action ("OnTap:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnTap (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (uiContentInput != null) {
                uiContentInput.Dispose ();
                uiContentInput = null;
            }

            if (uiDeleteButton != null) {
                uiDeleteButton.Dispose ();
                uiDeleteButton = null;
            }

            if (uiIsDoneSwitch != null) {
                uiIsDoneSwitch.Dispose ();
                uiIsDoneSwitch = null;
            }

            if (uiSaveButton != null) {
                uiSaveButton.Dispose ();
                uiSaveButton = null;
            }

            if (uiSelectImage != null) {
                uiSelectImage.Dispose ();
                uiSelectImage = null;
            }

            if (uiTaskImage != null) {
                uiTaskImage.Dispose ();
                uiTaskImage = null;
            }

            if (uiTitleInput != null) {
                uiTitleInput.Dispose ();
                uiTitleInput = null;
            }
        }
    }
}